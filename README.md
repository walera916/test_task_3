### Test task ayz

1) SQL part:
```SELECT p.date, SUM(p.quantity * pl.price) as sum_by_day FROM products p
   LEFT JOIN price_log pl ON p.product_id = pl.product_id
   WHERE pl.id = (
       SELECT id
       FROM price_log pl_i
       WHERE pl_i.date <= p.date AND pl_i.product_id = p.product_id
       GROUP BY id
       HAVING max(pl_i.date)
       ORDER BY id DESC
       LIMIT 1
   )
   GROUP BY p.date
   ORDER BY p.date ASC;
```

2) Web-scrapper: ```php index.php abc```