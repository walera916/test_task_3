<?php

namespace App\Parser;

use App\Client\HttpClient;
use App\Builder\AuthDtoBuilder;
use App\Builder\IpInfoBuilder;
use App\Dto\IpInfoDto;
use GuzzleHttp\Psr7\Uri;

final class Parser
{
    private HttpClient $client;
    public function __construct()
    {
        $this->client = new HttpClient();
    }

    /**
     * @param string $searchWord
     * @return IpInfoDto[]
     */
    public function parse(string $searchWord): array
    {
        $ipInfos = [];
        $authDto = AuthDtoBuilder::build($this->client->getAuthData());

        $firstSearchResponse = $this->client->getSearchResponse($authDto, $searchWord);

        $responseContent = $firstSearchResponse->getBody()->getContents();
        try {
            $doc = new \DOMDocument();
            @$doc->loadHTML($responseContent);
        } catch (\Throwable) {
            throw new \DomainException('Invalid response data');
        }


        $currentPage = -1;
        $paginationUrl = $this->getPaginationUrl($doc);

        if (null === $paginationUrl) {
            return [];
        }

        while ($this->pageHasNextPage($doc, $currentPage)) {
            $currentPage++;
            $paginationUrl = $this->buildNextPageUrl($paginationUrl, $currentPage);

            $paginationResponse = $this->client->getNextPageSearchResult($authDto, $paginationUrl);
            $responseContent = $paginationResponse->getBody()->getContents();
            try {
                $doc = new \DOMDocument();
                @$doc->loadHTML($responseContent);
            } catch (\Throwable) {
                throw new \DomainException('Invalid response data');
            }


            $ipInfos = \array_merge($ipInfos, $this->parsePage($doc));
        }

        return $ipInfos;
    }

    /**
     * @param \DOMDocument $document
     * @return IpInfoDto[]
     */
    private function parsePage(\DOMDocument $document): array
    {
        $result = [];
        $tableDom = $document->getElementById('resultsTable');

        if (null === $tableDom) {
            return [];
        }

        $tableRows = $tableDom->getElementsByTagName('tbody');
        foreach ($tableRows as $row) {
            $result[] = IpInfoBuilder::build($row);
        }

        return $result;
    }

    private function pageHasNextPage(\DOMDocument $document, int $currentPage): bool
    {
        $nodes = $this->findNodesByClass($document, 'right-aligned no-padding');

        /** @var \DOMElement $node */
        foreach ($nodes as $node) {
            foreach ($node->getElementsByTagName('a') as $aNode) {
                $pageNumber = $aNode->getAttribute('data-gotopage');
                if (null !== $pageNumber && (int)$pageNumber > $currentPage) {
                    return true;
                }
            }
        }

        return false;
    }

    private function buildNextPageUrl(string $pageUrl, int $page): string
    {
        $queryParams = [];
        /** @var array<mixed, mixed> $urlParts */
        $urlParts = parse_url($pageUrl);
        $uriObj = Uri::fromParts($urlParts);
        parse_str($uriObj->getQuery(), $queryParams);
        $queryParams['p'] = $page;
        $uriObj = $uriObj->withQuery(http_build_query($queryParams));

        return (string) $uriObj;
    }

    private function getPaginationUrl(\DOMDocument $document): ?string
    {
        $nodes = $this->findNodesByClass($document, 'right-aligned no-padding');

        /** @var \DOMElement $node */
        foreach ($nodes as $node) {
            foreach ($node->getElementsByTagName('a') as $aNode) {
                $linkHref = $aNode->getAttribute('href');
                if ('' !== $linkHref) {
                    return $linkHref;
                }
            }
        }

        return null;
    }

    /**
     * @param \DOMDocument $document
     * @param string $className
     * @return \DOMNodeList
     */
    private function findNodesByClass(\DOMDocument $document, string $className)
    {
        $finder = new \DomXPath($document);
        $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $className ')]");

        if (false === $nodes) {
            throw new \DomainException('Invalid response data');
        }

        return $nodes;
    }
}
