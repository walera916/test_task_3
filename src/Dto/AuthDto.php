<?php

namespace App\Dto;

final class AuthDto
{
    public function __construct(
        private readonly string $authCookie,
        private readonly string $csrf
    ) {
    }

    public function getAuthCookie(): string
    {
        return $this->authCookie;
    }

    public function getCsrf(): string
    {
        return $this->csrf;
    }
}
