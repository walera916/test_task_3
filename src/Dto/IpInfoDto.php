<?php

namespace App\Dto;

final class IpInfoDto
{
    public function __construct(
        public ?string $number = null,
        public ?string $urlDetailsPage = null,
        public ?string $name = null,
        public ?string $class = null,
        public ?string $status = null,
        public ?string $urlLogo = null,
    ) {
    }

    /**
     * @return array<string, string|null>
     */
    public function toArray(): array
    {
        return [
            'number' => $this->number,
            'url_logo' => $this->urlLogo,
            'name' => $this->name,
            'class' => $this->class,
            'status' => $this->status,
            'url_details_page' => $this->urlDetailsPage,
        ];
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function getUrlDetailsPage(): ?string
    {
        return $this->urlDetailsPage;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getUrlLogo(): ?string
    {
        return $this->urlLogo;
    }

}
