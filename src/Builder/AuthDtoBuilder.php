<?php

namespace App\Builder;

use App\Dto\AuthDto;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

final class AuthDtoBuilder
{
    public static function build(ResponseInterface $response): AuthDto
    {
        $cookieHeader = $response->getHeader('Set-Cookie');

        $authCookie = '';
        foreach ($cookieHeader as $cookieParam) {
            $authCookie .= $cookieParam . '; ';
        }

        $authResponse = $response->getBody()->getContents();
        try {
            $doc = new \DOMDocument();
            @$doc->loadHTML($authResponse);
        } catch (\Throwable) {
            throw new \DomainException('Invalid response data');
        }

        $csrf = null;
        $metas = $metas = $doc->getElementsByTagName('meta');
        foreach ($metas as $key => $meta) {
            if (strtolower($meta->getAttribute('name')) === '_csrf') {
                $csrf = $meta->getAttribute('content');
            }
        }

        if ('' === $authCookie || $csrf === null) {
            throw new \DomainException('Not found auth params in response');
        }

        return new AuthDto($authCookie, $csrf);
    }
}
