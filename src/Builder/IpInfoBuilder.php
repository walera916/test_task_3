<?php

namespace App\Builder;

use App\Dto\IpInfoDto;

final class IpInfoBuilder
{
    public static function build(\DOMElement $row): IpInfoDto
    {
        $ipInfoDto = new IpInfoDto();

        foreach ($row->getElementsByTagName('td') as $rowColumn) {
            $columnClass = $rowColumn->getAttribute('class');
            if ($columnClass === 'number') {
                $aItems = $rowColumn->getElementsByTagName('a');
                /** @var \DOMElement $aItem */
                foreach ($aItems as $aItem) {
                    $ipInfoDto->number = $aItem->nodeValue;
                    $ipInfoDto->urlDetailsPage = $aItem->getAttribute('href');
                }
            } elseif ($columnClass === 'trademark words') {
                $ipInfoDto->name = $rowColumn->nodeValue;
            } elseif ($columnClass === 'classes ') {
                $ipInfoDto->class = $rowColumn->nodeValue;
            } elseif ($columnClass === 'status') {
                $ipInfoDto->status = $rowColumn->nodeValue;
            } elseif ($columnClass === 'trademark image') {
                $imgItems = $rowColumn->getElementsByTagName('img');
                foreach ($imgItems as $imgItem) {
                    $ipInfoDto->urlLogo = $imgItem->getAttribute('src');
                }
            }
        }

        return $ipInfoDto;
    }
}
