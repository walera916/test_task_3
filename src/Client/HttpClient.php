<?php

namespace App\Client;

use App\Dto\AuthDto;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

final class HttpClient
{
    private const BASE_URL = 'https://search.ipaustralia.gov.au';
    private const AUTH_URI = '/trademarks/search/advanced';
    private const SEARCH_URI = '/trademarks/search/doSearch';

    private Client $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => self::BASE_URL]);
    }


    public function getAuthData(): ResponseInterface
    {
        return $this->client->get(self::AUTH_URI, [
            'headers' => [
                'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36',
            ]
        ]);
    }

    public function getSearchResponse(AuthDto $authDto, string $searchWord): ResponseInterface
    {
        return $this->client->post(self::SEARCH_URI, [
            'form_params' => [
                '_csrf' => $authDto->getCsrf(),
                'wv[0]' => $searchWord
            ],
            'headers' => [
                'cookie' => $authDto->getAuthCookie()
            ]
        ]);
    }

    public function getNextPageSearchResult(AuthDto $authDto, string $nextPageUri): ResponseInterface
    {
        return $this->client->get($nextPageUri, [
            'headers' => [
                'cookie' => $authDto->getAuthCookie()
            ]
        ]);
    }
}
