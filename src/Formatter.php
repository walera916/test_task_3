<?php

namespace App;

use App\Dto\IpInfoDto;

final class Formatter
{
    /**
     * @param IpInfoDto[] $items
     * @return string
     */
    public function format(array $items): string
    {
        $items = array_map(fn (IpInfoDto $dto) => $dto->toArray(), $items);

        return (string) json_encode($items);
    }
}
