<?php

require_once 'vendor/autoload.php';

$parser = new \App\Parser\Parser();
$formatter = new \App\Formatter();

$formattedResult = $formatter->format($parser->parse($argv[1]));

echo $formattedResult;